<?php

if(file_exists(__DIR__ . '/vendor/autoload.php')) {
	require __DIR__ . '/vendor/autoload.php';
}

// Create instance of our Router which extends bramus router in vendor
$router = new \ExpressPHP\Router();

// Define routes
 $router->get('/', function () {
       	echo 'here';
});

// Run it!
$router->run();